<?php
// ejemplos con variables superglobales
/* el autor menciona que es importante, tener cuidado al hacer uso de las
 * variables superglobales, esto devido a que suelen ser aprobechadas
 * para realizar ataques por lo que recomienda hacer uso de
 * el metodo ---- ->
 *                     htmlentities()
 * */

//$came_from=htmlentities($_SERVER['HTTP_REFERER']);
$came_from=htmlentities("ñañañña&5454545");
echo $came_from;

?>
